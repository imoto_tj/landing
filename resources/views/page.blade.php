@extends('layout')
@section('content')
    <section class="page-section">
    <div class="container">
        <div class="row">
            <div class="card mt-5">
                <div class="card-body">
                    <div class="card-title">
                        <h2 class="text-secondary mb-0">{{ $title }} </h2> <p>{{ $time }}</p>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam condimentum eget ex eget malesuada. Aenean elementum condimentum velit, non commodo diam viverra non. Fusce commodo purus quis sem dapibus, in viverra massa vestibulum. Cras eget ex nec augue tincidunt luctus. Cras non tellus vitae velit ornare sodales nec ut nibh. Nullam iaculis leo turpis, sit amet faucibus nulla gravida eu. Pellentesque eget metus velit. Vestibulum eu finibus ipsum. Integer blandit, ipsum vestibulum vehicula eleifend, elit ligula blandit eros, ut vehicula nisi magna sit amet nibh. Sed consectetur eget lacus sit amet auctor. Vestibulum justo mauris, tempor at bibendum ut, faucibus quis ligula.

                        Duis dictum sodales nisl, facilisis sagittis mauris scelerisque vel. Sed finibus finibus sapien, in mollis nunc. Quisque quis erat eget lectus lobortis luctus a eget felis. Suspendisse lobortis molestie congue. Ut hendrerit quam eu eros consequat, eget pharetra nisi volutpat. Nullam odio lorem, condimentum quis dapibus luctus, pretium eu nunc. Mauris maximus dolor felis, in vestibulum metus sagittis ac. Integer molestie purus vitae augue vulputate ullamcorper. Etiam ultricies faucibus lectus, sed feugiat sem tempus at. Suspendisse eget ante a sapien ullamcorper tincidunt. In eu bibendum mauris, vel sodales nisl. Sed rhoncus a ex a vehicula. Nullam pellentesque, dui et lobortis pulvinar, sapien mi sodales velit, et interdum turpis lectus in lectus.

                        Maecenas at mattis arcu. Proin tempus risus a augue ullamcorper condimentum. Morbi convallis justo quis velit sagittis aliquet. Sed rhoncus orci sagittis porta rhoncus. Nullam fermentum egestas convallis. Maecenas efficitur sagittis felis. Quisque posuere feugiat velit eu rutrum.</p>
                </div>
            </div>
            <a class="btn btn-secondary float-left" href="/#{{$back}}">Back</a>
        </div>

    </div>
    </section>
@endsection

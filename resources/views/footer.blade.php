<div class="copyright py-4 text-center text-white">
    <div class="container"><small>Copyright &copy; Landing 2021</small></div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('/js/scripts.js') }}"></script>

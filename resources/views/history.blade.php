@extends('layout')
@section('content')
    <section class="page-section">
        <div class="container">
            <div class="row">
                <div class="card mt-5">
                    <div class="card-body">
                        <div class="card-title">
                            <h2 class="text-secondary mb-0">History </h2>
                        </div>
                        <div class="card-description">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Урл</th>
                                    <th>Количество визитов</th>
                                    <th>Последнее посещение</th>

                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($data as $key => $val)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>/{{ $val['url'] }}</td>
                                        <td>{{ $val['total'] }}</td>
                                        <td>{{ $val['st_date'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                        {!! $data->links() !!}
                    </div>
                </div>
                <a class="btn btn-secondary float-left" href="/">Back</a>
            </div>
        </div>
    </section>

@endsection

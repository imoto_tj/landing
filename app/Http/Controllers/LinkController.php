<?php

namespace App\Http\Controllers;

use App\Services\JsonRpcClient;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class LinkController extends Controller
{
    protected $client;

    public function __construct(JsonRpcClient $client)
    {
        $this->client = $client;
    }

    public function index($back,$page){

        $time = now()->format('Y-m-d H:i:s');
        $data = $this->client->send('activity@store', ['url' => $page,'st_date'=>$time]);

        return view('page',compact('time','back','data'))->with('title','Page - '.$page);
    }

    public function activities(){

        $data = $this->client->send('activity@getdata', []);
        $data = $this->paginate(collect($data['result']));
        $data->withPath('/admin/activity');

        return view('history',compact('data'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
